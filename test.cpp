/*
 * test.cpp
 * Copyright (C) 2017 Aravind SUKUMARAN RAJAM (asr) <aravind_sr@outlook.com>
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#include "allocator.h"

#include <iostream>
#include <omp.h>

#define CNTR (1<<24)

int main(int  /*argc*/, char *[] /*argv*/)
{
    ASR::AlignedLinearAllocator<int,64> ala;

    double start_ala = omp_get_wtime( );

    #pragma omp parallel for
    for (uint64_t i = 0; i < CNTR; ++i)
    {
        int *ptr = ala.allocate(omp_get_thread_num(), 4);
        if((reinterpret_cast<size_t>(ptr) & (64-1)) !=0 ) {
            std::cout<< ptr << " " << (reinterpret_cast<size_t>(ptr) & (64-1))<< " alignment failed\n";
        }
    }

    double end_ala = omp_get_wtime( );
    std::cout << "ALa time " << end_ala - start_ala << std::endl;
    ala.deallocate();


    ASR::LinearAllocator<int> la;

    double start_la = omp_get_wtime( );

    #pragma omp parallel for

    for (uint64_t i = 0; i < CNTR; ++i)
    {
        la.allocate(omp_get_thread_num(), 4);
    }

    double end_la = omp_get_wtime( );
    std::cout << "La time " << end_la - start_la << std::endl;
    la.deallocate();


    ASR::DefaultAllocator<int> da;

    double start_da = omp_get_wtime( );

    #pragma omp parallel for

    for (uint64_t i = 0; i < CNTR; ++i)
    {
        da.allocate(4);
    }

    double end_da = omp_get_wtime( );
    std::cout << "da time " << end_da - start_da << std::endl;

    return 0;
}


