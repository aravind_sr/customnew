#
# Makefile
# Aravind SUKUMARAN RAJAM (asr), 2017-10-11 15:00
#

all: test

test: test.cpp
	g++ test.cpp -O3 -fopenmp -std=c++14 -o test
clean:
	rm -rf test

# vim:ft=make
#
