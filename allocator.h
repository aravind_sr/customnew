/*
 * allocator.h
 * Copyright (C) 2017 Aravind SUKUMARAN RAJAM (asr) <aravind_sr@outlook.com>
 *
 * Distributed under terms of the GNU LGPL3 license.
 */

#ifndef ALLOCATOR_H
#define ALLOCATOR_H

#include <array>
#include <cassert>
#include <iostream>
#include <memory>
#include <vector>

namespace ASR
{

/**
 * @brief: A helper class for freeing aligned alloc
 */
struct free_delete
{
    void operator()(void* x)
    {
        free(x);
    }
};

/**
* A simple linear allocator which always returns aligned address
*
* @note Thread Safety: The code is thread safe (ensure that MAX_THREADS variable is set to a valid value)
*/
template <typename T, size_t ALIGNMENT = 64, size_t MAX_THREADS = 72, size_t MAX_CNT = 2048>
class AlignedLinearAllocator
{
public:

    AlignedLinearAllocator() = default;
    AlignedLinearAllocator(const AlignedLinearAllocator &) = delete;

    /**
     * Allocates aligned memory for @in_num_elems elements
     * @param tid         : thread id
     * @param num_elems   :
     * @return            :
     */
    T* const allocate(size_t tid, size_t in_num_elems)
    {
        static_assert((ALIGNMENT & ALIGNMENT - 1) == 0, "Alignment should be a power of 2");

        auto additional = ( ALIGNMENT / sizeof(T) - in_num_elems % (ALIGNMENT / sizeof(T)) );
        size_t num_elems = in_num_elems + ((additional == ALIGNMENT / sizeof(T)) ? 0 : additional );

        if(cur_chunk_[tid] == nullptr || cur_chunk_cnt_[tid] + num_elems > MAX_CNT)
        {
            /**
             * If the number of elements requested is greater than MAX_CNT (1), then allocate space for
             * the number of elements requested.
             *
             * @note  If (1) is true, then the chunk will expire immediately. Also, each such call will
             * fallback to a "malloc", hence will not be efficient. To prevent this increase MAX_CNT
             *
             */
            int alloc_elems = std::max(MAX_CNT, num_elems);
            std::unique_ptr<T, free_delete> uptr(static_cast<T*>(aligned_alloc(ALIGNMENT, alloc_elems * sizeof(T))));
            cur_chunk_[tid] = uptr.get();
            chunk_list_[tid].push_back(std::move(uptr));
            cur_chunk_cnt_[tid] = 0;
        }

        auto tmp_ptr = cur_chunk_[tid] + cur_chunk_cnt_[tid];
        cur_chunk_cnt_[tid] += num_elems;
        return tmp_ptr;
    }

    /**
     * Deallocate the entire memory
     *
     * @note: No need to call this function explicitly
     */
    void deallocate()
    {
        for (size_t i = 0; i < MAX_THREADS; ++i)
        {
            cur_chunk_[i] = nullptr;
            cur_chunk_cnt_[i] = 0;
            chunk_list_[i].clear();
        }
    }

private:
    std::array<T*, MAX_THREADS> cur_chunk_{}; /// current chunk pointers
    std::array<size_t, MAX_THREADS> cur_chunk_cnt_{}; /// the number of elements in current chunk
    std::array<std::vector<std::unique_ptr<T, free_delete>>, MAX_THREADS> chunk_list_; /// the list of chunks already allocated
}; /* AlignedLinearAllocator*/


/**
 * A simple linear allocator
 *
 * @note Thread Safety: The code is thread safe (ensure that MAX_THREADS variable is set to a valid value)
 */
template <typename T, size_t MAX_THREADS = 72, size_t MAX_CNT = 2048>
class LinearAllocator
{
public:

    LinearAllocator() = default;
    LinearAllocator(const LinearAllocator &) = delete;

    /**
     * Allocates memory for @num_elems elements
     * @param tid         : thread id
     * @param num_elems   :
     * @return            :
     */
    T* const allocate(size_t tid, size_t num_elems)
    {
        if(cur_chunk_[tid] == nullptr || cur_chunk_cnt_[tid] + num_elems > MAX_CNT)
        {
            /**
             * If the number of elements requested is greater than MAX_CNT (1), then allocate space for
             * the number of elements requested.
             *
             * @note  If (1) is true, then the chunk will expire immediately. Also, each such call will
             * fallback to a "malloc", hence will not be efficient. To prevent this increase MAX_CNT
             *
             */
            int alloc_elems = std::max(MAX_CNT, num_elems);
            std::unique_ptr<T[]> uptr(new T[alloc_elems]);
            cur_chunk_[tid] = uptr.get();
            chunk_list_[tid].push_back(std::move(uptr));
            cur_chunk_cnt_[tid] = 0;
        }

        auto tmp_ptr = cur_chunk_[tid] + cur_chunk_cnt_[tid];
        cur_chunk_cnt_[tid] += num_elems;
        return tmp_ptr;
    }

    /**
     * Deallocate the entire memory
     *
     * @note: No need to call this function explicitly
     */
    void deallocate()
    {
        for (size_t i = 0; i < MAX_THREADS; ++i)
        {
            cur_chunk_[i] = nullptr;
            cur_chunk_cnt_[i] = 0;
            chunk_list_[i].clear();
        }
    }

private:
    std::array<T*, MAX_THREADS> cur_chunk_{}; /// current chunk pointers
    std::array<size_t, MAX_THREADS> cur_chunk_cnt_{}; /// the number of elements in current chunk
    std::array<std::vector<std::unique_ptr<T[]>>, MAX_THREADS> chunk_list_; /// the list of chunks already allocated
}; /* LinearAllocator*/

/**
 * A default allocator which is LEAKY!!!
 */

template <typename T>
class DefaultAllocator
{
public:
    T* const allocate(size_t num_elems)
    {
        return new T[num_elems];
    }
    void deallocate()
    {
        std::cout << "I dont take care of memory leak as I dont keep track of allocated memory\n";
    }
}; /* DefaultAllocator */

} // namespace ASR
/* ASR */

#endif /* !ALLOCATOR_H */
